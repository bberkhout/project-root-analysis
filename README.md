# Minor project semi-automated root analysis
### Bas & Benjamin 

### What is this README for?
* Brief summary
* Installation
* How to run the application

### Brief summary project
For this project, that was commissioned by Desiree den os, the main goal was to create an 
application that automatically performs a root elongation anaysis on microscopic Arabidopsis root 
images. The application uses a series of root images as input
and creates multiple types of visualizations as output. Some settings can
be changed to make the analysis perform better.

### Installation
To execute this application Python 3 has to be installed. 
The following python packages are also essential:

* OpenCV
* NumPy
* Matplotlib
* tkinter
* scipy

### How to run the application
* To set up this project, clone this repository to your pc
* Open a local terminal
	* Direct to the folder in which the cloned repository is located (this can be done using the 'cd' command)
	* Run the application by executing the command "python Gui.py"
* When the application is stared, choose a folder in which the series of root images of interest is located
* Change the settings to your preference
* Click Run Analysis
* When the analysis is finished, the output can be found in the TestData/output folder
