import cv2
import sys
import math
import VideoCreate

from scipy import ndimage


class TrackRootPoints:
    """
    Class that contains methods to calculate growth for different
    points on root images
    """
    def __init__(self, args):
        self.reference_delta = int(args['reff_delta'])
        self.grow_thresh = int(args['grow_threshold'])

    def calculate_growth(self, path, p1):
        """
        Method that calculates growth for an input box
        :param path: Path to base video
        :param p1: coordinates of box that will be tracked
        :return: list of growth values, list of x and y where the box passed
        """
        # Getting video from directory
        video = cv2.VideoCapture(path)

        ok, frame = video.read()
        if not ok:
            print("Could not read video")
            sys.exit()

        frame, angle = VideoCreate.VideoCreate.rotate(frame)

        # Set the delta value depending on the coordinate of the startpoint
        if p1[0] > self.reference_delta:
            self.reference_delta = self.reference_delta
        elif p1[0] < 40:
            self.reference_delta = 20
        else:
            self.reference_delta = 40

        # Create p2
        x1 = p1[0] - self.reference_delta
        y1 = p1[1]
        p2 = (x1, y1, p1[2], p1[3])

        # Creating CSRT tracker
        multiTracker = cv2.MultiTracker_create()

        # add both points to the tracker
        multiTracker.add(cv2.TrackerCSRT_create(), frame, p1)
        multiTracker.add(cv2.TrackerCSRT_create(), frame, p2)

        xs = []
        ys = []

        distances = []

        # Loop while there is a next frame
        while True:
            # New frame
            ok, frame = video.read()
            if not ok:
                break

            # rotate if angle is greater than 5
            if angle >= 5:
                frame = ndimage.rotate(frame, angle, reshape=True)

            # Update box
            ok, boxes = multiTracker.update(frame)

            end_points = []
            x = []
            y = []

            for i, newbox in enumerate(boxes):
                # Define the next point while tracking
                end_points.append((int(newbox[0] + newbox[2] / 2), int(newbox[1] + newbox[3] / 2)))
                x.append(end_points[i][0])
                y.append(end_points[i][1])

            # Calculate the distance between the two points
            distance = abs(round(math.sqrt(((end_points[1][1] - end_points[0][1]) ** 2) +
                                           ((end_points[1][0] - end_points[0][0]) ** 2)) - self.reference_delta))

            print(distance)
            distances.append(distance)
            # append coordinates to list depending on growth
            if distance >= self.grow_thresh:
                for _ in range(round((distance ** 2) * 0.10)):
                    for i in range(len(x)):
                        xs.append(x[i])
                        ys.append(y[i])

        return distances, xs, ys
