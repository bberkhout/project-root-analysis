import RecognizeRoot
import DetermineGrowth
import numpy as np
import cv2
import VideoCreate
from PIL import Image, ImageDraw
from tkinter import *
from tkinter import ttk
import tkinter as tk
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d
import pandas as pd
import glob


class GenerateOutput:
    """
    Class that contains methods to create output for the analysis
    """
    def __init__(self, path, args):
        self.pixel_size = 0.507
        self.path = path
        self.cfg = args
        self.images = sorted(glob.glob(self.path + "/*.TIF"))
        self.first_img_path = self.images[0]
        self.first_img = cv2.imread(self.first_img_path)
        self.last_image = self.images[-1:][0]

        print("Last image: ", self.last_image)
        print("First image: ", self.first_img_path)

    def run_boxes(self, fixed_reference=False):
        """
        Method that executes calcuate_growth for a list of boxes that are determined by create_boxes.
        Creates a csv file with the x coordinates and its corresponding growth
        :param fixed_reference:
        :return: list of x and y values that will be used in the heatmap, last image of the current series + shape
        """
        # create recognize root object to create boxes and tip coordinate
        recognize_root = RecognizeRoot.FindFeatures(self.cfg, self.first_img_path)
        boxes_grid, tip_coordinate = recognize_root.create_boxes()

        start_points = []

        # add startcoordinates to list
        for p in boxes_grid:
            start_point = (int(p[0] + p[2] / 2), int(p[1] + p[3] / 2))
            start_points.append(start_point)

        # img = cv2.imread("../../TestData/root1/120524th_sl8_pl2_wt_RELEL_00000.TIF")
        first_img, _ = VideoCreate.VideoCreate.rotate(self.first_img)

        x_axis = []

        # add a colored circle to the image
        for s in start_points:
            x_axis.append(s[0])
            cv2.circle(first_img, s, 4, [0, 255, 0], thickness=-1)

        # Create output of image with points to track
        first_img, _ = VideoCreate.VideoCreate.resize(first_img)
        cv2.imshow("points", first_img)
        # Press enter to continue tracking
        cv2.waitKey()
        print("continue tracking")
        cv2.destroyAllWindows()

        # write image to outputfolder
        cv2.imwrite("../../TestData/output/points_to_track.jpg", first_img)

        last_img = cv2.imread(self.last_image)
        # img = cv2.imread("../../TestData/root1/120524th_sl8_pl2_wt_RELEL_00000.TIF")
        # img, _ = VideoCreate.resize(img)
        last_img, _ = VideoCreate.VideoCreate.rotate(last_img)

        # get shape of the last image
        shape = last_img.shape
        perc = 1

        xs = []
        ys = []
        distances = []
        counter_bbox = 0

        # define DetermineGrowth object to calculate the growth
        trp = DetermineGrowth.TrackRootPoints(self.cfg)

        if fixed_reference:
            for bbox in boxes_grid:
                print("new box:")
                distance, x, y = trp.calc_growth_fixed_ref(output="../../TestData/videos/growth.avi",
                                                       path="../../TestData/videos/root_base.avi", p1=bbox,
                                                           tip=tip_coordinate)

                distances.append(max(distance))

                for i in range(len(x)):
                    xs.append(x[i])
                    ys.append(y[i])
                VideoCreate.VideoCreate.printProgressBar(perc + 1, len(boxes_grid), prefix='Tracking features:', suffix='Complete',
                                             length=50)
                perc += 1
        else:
            for bbox in boxes_grid:
                correct = False
                print("new box:")
                # Track current box
                distance, x, y = trp.calculate_growth(path="../../TestData/videos/root_base.avi", p1=bbox)
                print(distance)

                # First checks to find possible outliers of tracking
                if max(distance) - min(distance) > 30 or ((distance[-1] + 2) * 2) < max(distance):
                    counter = 0
                    print("Outlier found, creating new box to track")
                    # check if growth does not increase with impossible values
                    while not correct:
                        counter += 1
                        print("counter: ", counter)
                        correct = True
                        for i in range(len(distance)):
                            if distance[i] - distance[i - 1] > 10:
                                # Create new box to track
                                bbox = (bbox[0], bbox[1], bbox[2] - 2, bbox[3] - 2)
                                print("Tracking again with new box: ", bbox)
                                # Track new box
                                distance, x, y = trp.calculate_growth(path="../../TestData/videos/root_base.avi",
                                                                      p1=bbox)
                                correct = False
                                break
                        # if the box is resized 5 times, consider growth as 0
                        if counter == 5:
                            distance, x, y = [0], [0], [0]
                            correct = True

                # append max growth value to the distance list
                distances.append(max(distance) * 0.507)

                # append coordinates of tracking to output lists
                for i in range(len(x)):
                    xs.append(x[i])
                    ys.append(y[i])

                counter_bbox += 1
                # print current box
                print("at box", counter_bbox, " from ", len(boxes_grid))

        # write the output to a csv file
        csv_output = {"x": x_axis, "growth": distances}
        df = pd.DataFrame(csv_output, columns=['x', 'growth'])
        df.to_csv(r'../../TestData/output/growth_output.csv', index=False, header=True)

        return xs, ys, last_img, shape

    def create_heatoverlay(self, xs, ys, shape, frame, savename):
        """
        Method that creates a heatoverlay using lists of x and y coordinates,
        this overlay indicates in which areas the root
        elongates the most.
        :param xs: list of x coordinates
        :param ys: list of y coordinates
        :param shape: shape properties of the image
        :param frame: image on which the heatoverlay will be made
        :param savename: filename of the output file
        """
        popup = tk.Toplevel()
        tk.Label(popup, text="Creating heatmap: ").grid(row=1, column=0)

        # Setting up progress bar
        progress = 0
        progress_var = tk.DoubleVar()
        progress_bar = ttk.Progressbar(popup, variable=progress_var, maximum=100)
        progress_bar.grid(row=1, column=1)  # .pack(fill=tk.X, expand=1, side=tk.BOTTOM)
        popup.pack_slaves()

        progress_step = float(100.0 / len(xs))

        # Plotting the figure with the coordinates
        black_array = np.zeros((shape[0], shape[1], 3), np.uint8)
        for x, y in zip(xs, ys):
            x, y = int(x), int(y)
            black_array[y, x] = [255, 255, 255]
            black_array = cv2.blur(black_array, (3, 3))
            popup.update()
            progress += progress_step
            print("PROGRES: ", progress)
            progress_var.set(progress)

        # black_img = Image.fromarray(black_array)
        new_img = np.zeros(shape, np.uint8)
        # Brighten the image
        for y in range(shape[0]):
            for x in range(shape[1]):
                for c in range(shape[2]):
                    new_img[int(y), int(x), c] = np.clip(3 * black_array[int(y), x, c] + 95, 0, 255)

        # Applying opencv colormap
        color_map = cv2.applyColorMap(new_img, cv2.COLORMAP_JET)
        heatmap = cv2.addWeighted(frame, 0.5, color_map, 0.5, 0)
        self.create_legend()

        popup.destroy()
        cv2.imwrite("../../Testdata/output/" + savename + ".jpg", heatmap)

    def add_legend(self, heatmap):
        """
        method that adds a legend to an existing heatmap
        :param heatmap: path to the heatmap
        """
        self.create_legend()
        colorbar = cv2.imread("../../TestData/output/colorbar.jpg", -1)
        x_offset = y_offset = 100

        heatmap = cv2.imread(heatmap)

        heatmap[y_offset:y_offset + colorbar.shape[0], x_offset:x_offset + colorbar.shape[1]] = colorbar
        cv2.imwrite("heat_legend.jpg", heatmap)

    def create_legend(self):
        """
        Method that creates a legend for a heatmap
        :return: legend
        """
        df = pd.read_csv('../../TestData/output/growth_output.csv')
        growth = df["growth"]
        max_growth = max(growth)

        # gradient = np.linspace((-max_growth), max_growth, max_growth)
        gradient = np.linspace(0, 1, 256)
        gradient = np.vstack((gradient, gradient))
        print(gradient)

        fig, axes = plt.subplots(nrows=1, figsize=(15,15))
        fig.subplots_adjust(top=0.5, bottom=0.4, left=0.5, right=0.99)
        plt.xticks(np.arange(0, 256, step=8))
        # plt.xlim((max_growth/3, max_growth))
        labels = [item.get_text() for item in axes.get_xticklabels()]
        print(labels)
        label_length = int(len(labels))
        half_length = round(int((label_length-10)/2))
        axes.imshow(gradient, aspect="auto", cmap=plt.get_cmap("jet"))
        labels[half_length] = '0'
        labels[label_length-1] = str(max_growth)
        labels[label_length - round(label_length*0.25)] = str(max_growth/2)
        print(labels)
        axes.set_xticklabels(labels)
        axes.tick_params(labelsize=15)

        axes.get_yaxis().set_visible(False)
        # axes.set_axis_off()
        plt.savefig("../../TestData/output/colorbar.jpg", bbox_inches='tight')
        plt.show()
        return plt

    def create_graphs(self, first_img, graph=False, lnp=False):
        """
        Method that creates visualizations using the previously generated csv file
        :param graph: True if user wants normal graph
        :param lnp: True if user wants line next to image
        """
        # read data from csv file
        df = pd.read_csv('../../TestData/output/growth_output.csv')
        growth = df["growth"]
        xs = df["x"]

        # if line next to root is true
        if lnp:
            # smooth out growth data
            growth_smoothed = gaussian_filter1d(growth, sigma=4)
            line_coordinates = []
            # determine coordinates for the line on the image
            for g, x in zip(growth_smoothed, xs):
                line_coordinates.append([x, (700 - (int(g) ** 2))])

            # convert to numpy array
            line_coordinates = np.array([np.array(x) for x in line_coordinates])
            print(line_coordinates)

            # read first image
            image = cv2.imread(first_img)
            image, _ = VideoCreate.VideoCreate.rotate(image)

            # set line thickness and color of the line
            thickness = 2
            color = (255, 0, 0)

            # reshape the coordinates
            line_coordinates = line_coordinates.reshape((-1, 1, 2))

            # add the line to the image
            image = cv2.polylines(image, [line_coordinates],
                                  False, color, thickness)

            # write output
            cv2.imwrite('../../TestData/output/line_visualization.jpg', image)

        # if graph is true
        if graph:
            growth_micro = []
            for g in growth:
                # convert the unit to micrometers
                growth_micro.append(g*0.507)

            # smooth out growth data
            growth_smoothed = gaussian_filter1d(growth_micro, sigma=4)
            growth_smoothed = [growth_smoothed[i:i+7] for i in range(0, len(growth_smoothed), 7)]

            plt.plot(xs, growth_smoothed[0])
            plt.ylabel("growth in µm")
            plt.xlabel("x coordinate of the root")
            plt.title("Graph showing growth at x-coordinate of root image")
            plt.savefig("../../TestData/output/growth_graph.jpg", bbox_inches='tight')
