#!/usr/bin/env python
import glob
from tkinter import *
from tkinter import messagebox
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from PIL import ImageTk, Image
from tkinter import font as tkfont
import config as cfg
import cv2 as cv
import GenerateOutput
from os import path
import VideoCreate

LARGE_FONT = ("Verdana", 12)
NORM_FONT = ("Helvetica", 10)
SMALL_FONT = ("Helvetica", 8)
new_settings = cfg.settings
default_settings = cfg.default
heatmap_savename = ""


def settingspop():
    popup = tk.Tk()
    popup.wm_title("Settings")
    popup.minsize(200, 300)
    args = []
    v = IntVar()

    reff_point_setting = ttk.Label(popup, text="Reference point distance")
    reff_point_setting.grid(column=0, row=5, sticky=W, pady=4)
    reff_point_entry = Entry(master=popup)
    reff_point_entry.grid(column=5, row=5, sticky=E, pady=4)
    reff_point_entry.insert(0, new_settings['reff_delta'])
    args.append(reff_point_entry)

    grid_delta_setting = ttk.Label(popup, text="Distance between points on grid")
    grid_delta_setting.grid(column=0, row=6, sticky=W, pady=4)
    grid_delta_entry = Entry(master=popup)
    grid_delta_entry.grid(column=5, row=6, sticky=E, pady=4)
    grid_delta_entry.insert(0, new_settings['grid_point_distance'])
    args.append(grid_delta_entry)

    growth_thresh_setting = ttk.Label(popup, text="Growth Threshold for counting")
    growth_thresh_setting.grid(column=0, row=7, sticky=W, pady=4)
    growth_thresh_entry = Entry(master=popup)
    growth_thresh_entry.grid(column=5, row=7, sticky=E, pady=4)
    growth_thresh_entry.insert(0, new_settings['grow_threshold'])
    args.append(growth_thresh_entry)

    bbox_size_setting = ttk.Label(popup, text="Region of Interest size")
    bbox_size_setting.grid(column=0, row=8, sticky=W, pady=4)
    bbox_size_entry = Entry(master=popup)
    bbox_size_entry.grid(column=5, row=8, sticky=E, pady=4)
    bbox_size_entry.insert(0, new_settings['bbox_size'])
    args.append(bbox_size_entry)

    number_of_lines_setting = ttk.Label(popup, text="Amount of lines on the grid")
    number_of_lines_setting.grid(column=0, row=9, sticky=W, pady=4)
    number_of_lines_entry = Entry(master=popup)
    number_of_lines_entry.grid(column=5, row=9, sticky=E, pady=4)
    number_of_lines_entry.insert(0, new_settings['number_of_lines'])
    args.append(number_of_lines_entry)

    # Apply changes button
    apply = ttk.Button(popup, text="Apply", command=lambda: check_valid_argument(args))
    apply.grid(column=4, row=11, sticky=S)

    # Exit the settings interface without saving changes
    exit_button = ttk.Button(popup, text="Exit without saving", command=popup.destroy)
    exit_button.grid(column=1, row=11, sticky=S)

    restore_default_button = ttk.Button(popup, text="Restore to defaults", command=lambda: restore_defaults(args))
    restore_default_button.grid(column=2, row=11, sticky=S)

    popup.mainloop()


def restore_defaults(entries):
    print("restoring defaults")
    for key, entry in zip(default_settings, entries):
        try:
            new_settings[key] = default_settings[key]
            entry.delete(0, "end")
            entry.insert(0, new_settings[key])
        except AttributeError:
            pass


def apply_settings(args):
    for key, entry in zip(cfg.settings, args):
        new_settings[key] = entry.get()
    with open('config.py', 'w+') as f:
        f.write("settings = " + str(new_settings) + "\n" +
                "default = " + str(default_settings))


def check_valid_argument(entries):
    valid_arg = True
    for entry in entries:
        setting = entry.get()
        print(type(setting))
        try:
            print(setting)
            if setting is "" or None:
                tk.messagebox.showinfo("Warning", "Empty text field, make sure all fields are complete")
                valid_arg = False
            elif re.match(r'^-?\d+(?:\.\d+)?$', setting) is None:
                tk.messagebox.showinfo("Warning", "Input must be an integer")
                valid_arg = False
            else:
                pass
        except TypeError:
            print(setting)

    if valid_arg:
        apply_settings(entries)


class Interface(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.winfo_toplevel().title("Root analyzer")

        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")

        container = tk.Frame(self)
        # container.pack(side="top", fill="both", expand=True)
        container.grid(column=0, row=0)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartPage, Results):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("StartPage")

    def menubar(self, root):
        menubar = tk.Menu(root)
        pageMenu = tk.Menu(menubar)
        pageMenu.add_command(label="PageOne")
        menubar.add_cascade(label="PageOne", menu=pageMenu)
        return menubar

    def show_frame(self, page_name):
        '''Show a frame for the given page name'''
        frame = self.frames[page_name]
        frame.tkraise()

        menubar = frame.menubar(self)
        self.configure(menu=menubar)


class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        # controller.iconbitmap("../../TestData/download.ico")
        label = tk.Label(self, text="Click browse folder to start analysis", font=controller.title_font)
        label.grid(column=0, row=0)

        self.setup()

    def menubar(self, root):
        menu = tk.Menu(root)
        # create the file object)
        file = Menu(menu)
        analyse = Menu(menu)
        edit = Menu(menu)

        analyse.add_command(label="Go to analysis", command=lambda: self.controller.show_frame("StartPage"))
        menu.add_cascade(label="Analyse", menu=analyse)

        file.add_command(label="Show results", command=lambda: self.controller.show_frame("Results"))

        menu.add_cascade(label="Results", menu=file)

        edit.add_command(label="Edit parameters", command=lambda: settingspop())

        menu.add_cascade(label="Settings", menu=edit)
        return menu

    def setup(self):
        self.graph_var = tk.IntVar()
        self.lnp_var = tk.IntVar()
        options = []
        label_br = Label(self, text="Please select the folder")
        label_br.grid(column=0, row=1, sticky=W)

        button = Button(self, text="Browse A File", command=self.fileDialog)
        button.grid(column=1, row=1, sticky=E, padx=5)

        self.savename = Entry(self)
        self.savename.grid(column=1, row=3, sticky=E, pady=2, padx=5)
        self.savename.insert(0, "heatmap")
        global heatmap_savename
        heatmap_savename = self.savename.get()

        fileLabel = Label(self, text="Save name for heatmap image: ")
        fileLabel.grid(column=0, row=3, sticky=W, pady=2, padx=5)

        graph_type_label = Label(self, text="Graph type: ")
        graph_type_label.grid(column=0, row=4, sticky=W, pady=2, padx=5)

        graph_check = Checkbutton(self, text="Graph", variable=self.graph_var)
        graph_check.grid(column=1, row=4, sticky=E, pady=2, padx=5)

        lnp_check = Checkbutton(self, text="On root", variable=self.lnp_var)
        lnp_check.grid(column=2, row=4, sticky=E, pady=2, padx=5)

    def fileDialog(self):
        self.filename = filedialog.askdirectory()
        self.label = Label(self, text="")
        self.label.grid(column=0, row=2)
        self.label.configure(text=self.filename)
        self.uploadbutton = Button(self, text="Analyse", command=self.runAnalysis)
        self.uploadbutton.grid(column=0, row=5, sticky=S)
        print(self.filename)

    def runAnalysis(self):
        if self.graph_var.get() == 1:
            graph = True
        if self.graph_var.get() == 0:
            graph = False

        if self.lnp_var.get() == 1:
            lnp = True
        if self.lnp_var.get() == 0:
            lnp = False

        VideoCreate.VideoCreate.createvideo(title="root_base.avi", path="{}/*.TIF".format(self.filename))
        output_generator = GenerateOutput.GenerateOutput(self.filename, args=new_settings)
        xs, ys, frame, shape = output_generator.run_boxes()
        output_generator.create_heatoverlay(xs, ys, shape, frame, savename=self.savename.get())
        first_img = sorted(glob.glob(self.filename + "/*.TIF"))[0]
        output_generator.create_graphs(first_img, graph=graph, lnp=lnp)
        output_generator.add_legend("../../Testdata/output/heatmap_w_legend.jpg")

        # VideoCreate.createvideo("root_base.avi", path="{}/*.TIF".format(self.filename))
        # xs, ys, frame, shape = DetDiff.collect_data(self.filename)
        # Heatmap.heatoverlay(xs, ys, shape, frame, savename=self.savename.get())
        # Heatmap.create_legend()
        print(new_settings)


def showheatmap(heatmap_savename):
    heatmap = cv.imread("../../Testdata/output/" + heatmap_savename + ".jpg")
    if heatmap is not None:
        res_img, _ = VideoCreate.VideoCreate.resize(heatmap)
        cv.imshow("Heatmap results", res_img)
    else:
        tk.messagebox.showinfo("Warning", "No heatmap image found, go to analysis to start analysing")


def showgraph():
    graph = cv.imread("../../TestData/output/growth_graph.jpg")
    if graph is not None:
        cv.imshow("Growth Graph", graph)
    else:
        tk.messagebox.showinfo("Warning", "No graph image found, go to analysis to start analysing")


def lineoverlay():
    graph = cv.imread('../../TestData/output/line_visualization.jpg')
    if graph is not None:
        res_img, _ = VideoCreate.VideoCreate.resize(graph)
        cv.imshow("Growth Graph", res_img)
    else:
        tk.messagebox.showinfo("Warning", "No image found, go to analysis to start analysing")


class Results(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Results", font=controller.title_font)
        label.grid(column=0, row=0)

        showheatmap_button = Button(self, text="Show heatmap image result",
                                    command=lambda: showheatmap(heatmap_savename))
        showheatmap_button.grid(column=0, row=4, sticky=E, padx=6)

        showgraph_button = Button(self, text="Show growth graph", command=showgraph)
        showgraph_button.grid(column=2, row=4, sticky=E, padx=6)

        showoverlay_button = Button(self, text="Show growth graph", command=lineoverlay)
        showoverlay_button.grid(column=4, row=4, sticky=E, padx=6)

    def menubar(self, root):
        menu = tk.Menu(root)
        # create the file object)
        analyse = Menu(menu)
        edit = Menu(menu)

        menu.add_cascade(label="Analyse", menu=analyse)
        analyse.add_command(label="Analyse", command=lambda: self.controller.show_frame("StartPage"))

        edit.add_command(label="Edit parameters", command=lambda: settingspop())

        menu.add_cascade(label="Settings", menu=edit)
        return menu


if __name__ == "__main__":
    # Interface creation
    window = Interface()
    window.mainloop()


