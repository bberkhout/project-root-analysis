import cv2
import VideoCreate
import numpy as np
import math
from scipy import ndimage


class FindFeatures:
    """
    class that has methods for recognizing root properties and contours, with these
    properties a grid of boxes can be created.
    """
    def __init__(self, args, path):
        self.box_size = int(args['bbox_size'])
        self.grid_point_distance = int(args['grid_point_distance'])
        self.number_of_lines = int(args['number_of_lines'])
        self.path = path

    def find_properties(self):
        """
        Find properties of the root; diamter, length, tip coordinate and contour
        :return: diamter, length, tip_coordinate
        """
        # Load image, grayscale, Gaussian blur, threshold
        image = cv2.imread(self.path)
        image, angle = VideoCreate.VideoCreate.rotate(image)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (3, 3), 0)
        thresh = cv2.adaptiveThreshold(blur, 200, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                       cv2.THRESH_BINARY_INV, 19, 1.5)

        # Find contours of the root
        contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[0] if len(contours) == 2 else contours[1]
        contour = max(contours, key=cv2.contourArea)

        # Obtain tip_coordinate coordinate
        tip_coordinate = tuple(contour[contour[:, :, 0].argmax()][0])

        # get the length of the root
        length = tip_coordinate[0]

        # crop the image to a smaller scale
        x = tip_coordinate[0] - 700
        y = tip_coordinate[1] - 300
        h = 400
        w = 300
        thresh_cropped = thresh[y:y + h, x:x + w]
        image_cropped = image[y:y + h, x:x + w]

        # Find contours for top and bottom of root
        contours_cropped = cv2.findContours(thresh_cropped, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours_cropped = contours_cropped[0] if len(contours_cropped) == 2 else contours_cropped[1]
        c_cropped = max(contours_cropped, key=cv2.contourArea)

        # add top and bottom to cropped image
        top = tuple(c_cropped[c_cropped[:, :, 1].argmin()][0])
        bottom = tuple(c_cropped[c_cropped[:, :, 1].argmax()][0])

        # Draw dots onto image
        cv2.drawContours(image, [contour], -1, (36, 255, 12), 2)
        cv2.circle(image, tip_coordinate, 8, (0, 255, 255), -1)
        cv2.circle(image_cropped, top, 8, (255, 50, 0), -1)
        cv2.circle(image_cropped, bottom, 8, (255, 255, 0), -1)

        # Calculate diameter of the root
        diameter = abs(top[1] - bottom[1])
        print("diameter: ", diameter)

        print('tip_coordinate: {}'.format(tip_coordinate))
        cv2.imwrite('../../TestData/output/root_contour.jpg', image)

        return diameter, length, tip_coordinate, contour

    def create_boxes(self):
        """
        method for creating a grid of boxes using the properties of the root
        :return: boxes_grid, tip_coordinate
        """
        diameter, length, tip_coordinate, contour = self.find_properties()

        # calculate diameter of the root
        diameter_lines = round(diameter / self.number_of_lines)
        length_lines = int(round(length / self.grid_point_distance))

        bboxes = [(float(tip_coordinate[0]) - (self.box_size/2), float(tip_coordinate[1]) - (self.box_size/2), self.box_size,
                   self.box_size)]

        whole_root = False
        one_line = True

        # Create the list with starting points that will be tracked
        if whole_root:
            starts = []
            for i in range(6):
                for n in range(length_lines):
                    starts.append((bboxes[0][0] - (self.grid_point_distance * n), bboxes[0][1] - i * diameter_lines,
                                   bboxes[0][2], bboxes[0][3]))

            for i in range(2):
                for n in range(length_lines):
                    starts.append((bboxes[0][0] - (self.grid_point_distance * n), bboxes[0][1] + i * diameter_lines,
                                   bboxes[0][2], bboxes[0][3]))

        if one_line:
            # Create one line of starting points
            starts = [(bboxes[0][0] - (self.grid_point_distance * n), bboxes[0][1], bboxes[0][2],
                       bboxes[0][3]) for n in range(length_lines)]

        boxes_grid = []
        #
        for point in starts:
            x1 = cv2.pointPolygonTest(contour, (point[0], point[1]), False)
            if x1 == 1:
                boxes_grid.append(point)

        print(boxes_grid)
        print("amount of points to track: ", len(boxes_grid))

        return boxes_grid, tip_coordinate
