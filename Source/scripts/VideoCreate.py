import glob
import sys
import math
import numpy as np
import cv2
from scipy import ndimage


class VideoCreate:
    """
    class that contains methods for creating videos, rotating images and resizing images
    """
    def createvideo(title, fps=15, imgs=None, path=None):
        """
        create video from input images
        :param fps: frames per second of the output video
        :param imgs: input images
        :param path: path to input images
        :return: output video
        """
        images = []
        size = 0
        print(path)

        # Add every photo in directory to array
        if path:
            for filename in sorted(glob.glob(path)):
                img = cv2.imread(filename)

                height, width, layers = img.shape
                size = (width, height)

                images.append(img)
        else:
            for image in imgs:
                height, width, layers = image.shape
                size = (width, height)

                images.append(image)

        vid_name = '../../TestData/videos/' + title

        print("Making: ", vid_name)
        print(size)
        out = cv2.VideoWriter(vid_name, cv2.VideoWriter_fourcc(*'DIVX'), fps, size)

        # Write each image to videowriter
        for i in range(len(images)):
            out.write(images[i])

        return out

    def resize(frame):
        """
        resizing method
       :return: resized image
       """
        scale_percent = 40
        width = int(frame.shape[1] * scale_percent / 100)
        height = int(frame.shape[0] * scale_percent / 100)
        dim = (width, height)

        frame = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
        return frame, dim

    def rotate(image):
        """
        rotation method
        :return: rotated image
        """
        img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        img_edges = cv2.Canny(img_gray, 75, 75, apertureSize=3)
        lines = cv2.HoughLinesP(img_edges, 1, math.pi / 180.0, 100, minLineLength=100, maxLineGap=5)
        h, w = image.shape[:2]
        center = (w / 2, h / 2)

        angles = []

        for i in range(len(lines)):
            for x1, y1, x2, y2 in lines[i]:
                # cv2.line(image, (x1, y1), (x2, y2), (255, 0, 0), 3)
                angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
                angles.append(angle)

        median_angle = np.median(angles)
        print("median_angle = ", median_angle)
        if median_angle <= 5:
            median_angle = 0.0
        img_rotated = ndimage.rotate(image, median_angle, reshape=True)
        # rev_rotated = ndimage.rotate(img_rotated, 360 - median_angle, reshape=False)

        # print("Angle is {}".format(median_angle))
        # cv2.imwrite('rotated.jpg', img_rotated)
        # cv2.imwrite('reverse_rot.jpg', rev_rotated)

        return img_rotated, median_angle

    def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=printEnd)
        # Print New Line on Complete
        if iteration == total:
            print()
